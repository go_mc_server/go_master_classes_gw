package main

import (
	_ "go_master_classes_gw/docs"
	"go_master_classes_gw/internal/pkg"
)

// @title Swagger Example API
// @version 1.0
// @description This is a sample server celler server.

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @host localhost:8080
// @BasePath /

func main() {
	pkg.StartApplication()
}
