package converters

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"log"
)

func ConverterInNewMasterClassesDTO(
	masterClasses *master_classes.NewMasterClassDTO) (*dto.NewMasterClassesDTO, error) {
	var newMasterClassesDTO dto.NewMasterClassesDTO

	if err := mapstructure.Decode(masterClasses, &newMasterClassesDTO); err != nil {
		log.Printf("ConverterInNewMasterClassesDTO: %v", err)
		return nil, mistakes.ErrConversion
	}
	newMasterClassesDTO.CreatedAt = masterClasses.CreatedAt.AsTime()
	newMasterClassesDTO.UpdatedAt = masterClasses.UpdatedAt.AsTime()

	return &newMasterClassesDTO, nil
}

func ConverterInListMasterClassesDTO(
	masterClasses []*master_classes.MasterClassesDTO) ([]*dto.MasterClassesDTO, error) {
	var listMasterClassesDTO []*dto.MasterClassesDTO
	for _, masterClass := range masterClasses {
		var MasterClassDTO dto.MasterClassesDTO
		if err := mapstructure.Decode(masterClass, &MasterClassDTO); err != nil {
			log.Printf("ConverterInListMasterClassesDTO: %v", err)
			return nil, mistakes.ErrConversion
		}
		listMasterClassesDTO = append(listMasterClassesDTO, &MasterClassDTO)
	}
	return listMasterClassesDTO, nil
}

func ConverterInMasterClassDTO(
	masterClass *master_classes.MasterClassDTO) (*dto.MasterClassDTO, error) {
	var masterClassDTO dto.MasterClassDTO
	if err := mapstructure.Decode(masterClass, &masterClassDTO); err != nil {
		log.Printf("ConverterInMasterClassDTO: %v", err)
		return nil, mistakes.ErrConversion
	}
	masterClassDTO.CreatedAt = masterClass.CreatedAt.AsTime()
	masterClassDTO.UpdatedAt = masterClass.UpdatedAt.AsTime()

	return &masterClassDTO, nil
}
