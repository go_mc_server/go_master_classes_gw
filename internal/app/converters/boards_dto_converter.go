package converters

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"log"
)

func ConverterInBoardDTO(
	board *boards.BoardDTO) (*dto.BoardDTO, error) {
	var newBoardDTO dto.BoardDTO
	if err := mapstructure.Decode(board, &newBoardDTO); err != nil {
		log.Printf("ConverterInBoardDTO error: %v", err)
		return nil, mistakes.ErrConversion
	}
	newBoardDTO.CreatedAt = board.CreatedAt.AsTime()
	newBoardDTO.UpdatedAt = board.UpdatedAt.AsTime()

	return &newBoardDTO, nil
}

func ConverterInListBoardsDTO(
	boards []*boards.BoardsDTO) ([]*dto.BoardsDTO, error) {
	var listBoardsDTO []*dto.BoardsDTO
	for _, board := range boards {
		var boardDTO dto.BoardsDTO
		if err := mapstructure.Decode(board, &boardDTO); err != nil {
			log.Printf("ConverterInListBoardsDTO error: %v", err)
			return nil, mistakes.ErrConversion
		}
		listBoardsDTO = append(listBoardsDTO, &boardDTO)
	}
	return listBoardsDTO, nil
}

func ConverterInNewBoardDTO(
	board *boards.NewBoardDTO) (*dto.NewBoardDTO, error) {
	var newBoardDTO dto.NewBoardDTO
	if err := mapstructure.Decode(board, &newBoardDTO); err != nil {
		log.Printf("ConverterInNewBoardDTO error: %v", err)
		return nil, mistakes.ErrConversion
	}
	newBoardDTO.CreatedAt = board.CreatedAt.AsTime()
	newBoardDTO.UpdatedAt = board.UpdatedAt.AsTime()

	return &newBoardDTO, nil
}
