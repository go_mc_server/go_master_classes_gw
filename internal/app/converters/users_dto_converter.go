package converters

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"log"
)

func ConverterInUserDTO(user *users.UserDTO) (*dto.UserDTO, error) {
	var userDTO dto.UserDTO
	if err := mapstructure.Decode(user, &userDTO); err != nil {
		log.Printf("ConverterInUserDTO error: %v", err)
		return nil, mistakes.ErrConversion
	}
	userDTO.CreatedAt = user.CreatedAt.AsTime()
	userDTO.UpdatedAt = user.UpdatedAt.AsTime()

	return &userDTO, nil
}

func ConverterInListUsersDTO(users []*users.UsersDTO) ([]*dto.UsersDTO, error) {
	var listUsers []*dto.UsersDTO
	for _, user := range users {
		var usersDTO dto.UsersDTO
		if err := mapstructure.Decode(user, &usersDTO); err != nil {
			log.Printf("ConverterInListUsersDTO error: %v", err)
			return nil, mistakes.ErrConversion
		}
		listUsers = append(listUsers, &usersDTO)
	}
	return listUsers, nil
}

func ConverterInNewUserDTO(user *users.NewUserDTO) (*dto.NewUserDTO, error) {
	var newUserDTO dto.NewUserDTO
	if err := mapstructure.Decode(user, &newUserDTO); err != nil {
		log.Printf("ConverterInNewUserDTO error: %v", err)
		return nil, mistakes.ErrConversion
	}
	newUserDTO.CreatedAt = user.CreatedAt.AsTime()
	newUserDTO.UpdatedAt = user.UpdatedAt.AsTime()

	return &newUserDTO, nil
}
