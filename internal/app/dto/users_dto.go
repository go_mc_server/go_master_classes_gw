package dto

import (
	"go_master_classes_gw/internal/app/enum"
	"time"
)

type FiltersUsers struct {
	LastName   string `json:"last_name,omitempty"`
	FirstName  string `json:"first_name,omitempty"`
	MiddleName string `json:"middle_name,omitempty"`
	Email      string `json:"email,omitempty"`
	Role       string `json:"role,omitempty"`
}

type UserLoginDetails struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserDTO struct {
	ID         uint           `json:"id"`
	LastName   string         `json:"lastName"`
	FirstName  string         `json:"firstName"`
	MiddleName string         `json:"middleName"`
	Email      string         `json:"email"`
	Role       enum.RolesUser `json:"role"`
	CreatedAt  time.Time      `json:"createdAt"`
	UpdatedAt  time.Time      `json:"updatedAt"`
}

type UsersDTO struct {
	ID         uint           `json:"id"`
	LastName   string         `json:"lastName"`
	FirstName  string         `json:"firstName"`
	MiddleName string         `json:"middleName"`
	Email      string         `json:"email"`
	Role       enum.RolesUser `json:"role"`
}

type UsersWithoutRoleDTO struct {
	ID         uint   `json:"id"`
	LastName   string `json:"lastName"`
	FirstName  string `json:"firstName"`
	MiddleName string `json:"middleName"`
	Email      string `json:"email"`
}

type CreateUserDTO struct {
	LastName   string `json:"lastName"`
	FirstName  string `json:"firstName"`
	MiddleName string `json:"middleName,omitempty"`
	Password   string `json:"password"`
	Email      string `json:"email"`
}

type UpdateUserDTO struct {
	ID         uint64 `json:"id"`
	LastName   string `json:"lastName,omitempty"`
	FirstName  string `json:"firstName,omitempty"`
	MiddleName string `json:"middleName,omitempty"`
	Email      string `json:"email,omitempty"`
	Password   string `json:"password,omitempty"`
	Role       string `json:"role,omitempty"`
}

type UpdateUserRoleDTO struct {
	ID   uint           `json:"id"`
	Role enum.RolesUser `json:"role,omitempty"`
}

type UpdateMyselfDTO struct {
	LastName   string `json:"lastName,omitempty"`
	FirstName  string `json:"firstName,omitempty"`
	MiddleName string `json:"middleName,omitempty"`
	Email      string `json:"email,omitempty"`
	Password   string `json:"password,omitempty"`
}

type UpdatePasswordDTO struct {
	Email     string `json:"email"`
	LastName  string `json:"lastName"`
	FirstName string `json:"firstName"`
	Password  string `json:"password"`
}

type NewUserDTO struct {
	ID         uint           `json:"id"`
	LastName   string         `json:"lastName"`
	FirstName  string         `json:"firstName"`
	MiddleName string         `json:"middleName"`
	Email      string         `json:"email"`
	Password   string         `json:"password"`
	Role       enum.RolesUser `json:"role"`
	CreatedAt  time.Time      `json:"createdAt"`
	UpdatedAt  time.Time      `json:"updatedAt"`
}

type UserCreationResponseDTO struct {
	Message      string     `json:"message"`
	User         NewUserDTO `json:"user"`
	AccessToken  string     `json:"accessToken,omitempty"`
	RefreshToken string     `json:"refreshToken,omitempty"`
}
