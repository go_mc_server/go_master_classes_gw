package dto

import (
	"go_master_classes_gw/internal/app/enum"
	"time"
)

type FiltersMasterClasses struct {
	AuthorID       uint64 `json:"author_id,omitempty"`
	TypeNeedlework string `json:"type_needlework,omitempty"`
	Name           string `json:"name,omitempty"`
	Materials      string `json:"materials,omitempty"`
	IsActive       *bool  `json:"is_active,omitempty"`
}

type UpdateMasterClassDTO struct {
	ID             uint64              `json:"id"`
	TypeNeedlework enum.TypeNeedlework `json:"typeNeedlework,omitempty"`
	Name           string              `json:"name,omitempty"`
	Description    string              `json:"description,omitempty"`
	Materials      string              `json:"materials,omitempty"`
	DetailInfo     string              `json:"detailInfo,omitempty"`
	IsActive       *bool               `json:"isActive,omitempty"`
}

type MasterClassesDTO struct {
	ID             uint                `json:"id"`
	AuthorID       uint                `json:"author_id"`
	TypeNeedlework enum.TypeNeedlework `json:"typeNeedlework"`
	Name           string              `json:"name"`
	Description    string              `json:"description"`
	IsActive       bool                `json:"isActive"`
}

type NewMasterClassesDTO struct {
	ID             uint                `json:"id"`
	AuthorID       uint                `json:"authorId"`
	TypeNeedlework enum.TypeNeedlework `json:"typeNeedlework"`
	Name           string              `json:"name"`
	Description    string              `json:"description"`
	Materials      string              `json:"materials"`
	DetailInfo     string              `json:"detailInfo"`
	IsActive       *bool               `json:"isActive"`
	CreatedAt      time.Time           `json:"createdAt"`
	UpdatedAt      time.Time           `json:"updatedAt"`
}

type MasterClassDTO struct {
	ID             uint                `json:"id"`
	Author         UsersWithoutRoleDTO `json:"author"`
	TypeNeedlework enum.TypeNeedlework `json:"typeNeedlework"`
	Name           string              `json:"name"`
	Description    string              `json:"description"`
	Materials      string              `json:"materials"`
	DetailInfo     string              `json:"detailInfo"`
	IsActive       bool                `json:"isActive"`
	CreatedAt      time.Time           `json:"createdAt"`
	UpdatedAt      time.Time           `json:"updatedAt"`
}

type CreateMasterClassDTO struct {
	TypeNeedlework enum.TypeNeedlework `json:"typeNeedlework"`
	Name           string              `json:"name"`
	Description    string              `json:"description"`
	Materials      string              `json:"materials"`
	DetailInfo     string              `json:"detailInfo"`
}

type MasterClassNoDateDTO struct {
	ID             uint                `json:"id"`
	AuthorID       uint                `json:"author_id"`
	TypeNeedlework enum.TypeNeedlework `json:"typeNeedlework"`
	Name           string              `json:"name"`
	Description    string              `json:"description"`
	Materials      string              `json:"materials"`
	DetailInfo     string              `json:"detailInfo"`
	IsActive       bool                `json:"isActive"`
}
