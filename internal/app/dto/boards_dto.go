package dto

import (
	"go_master_classes_gw/internal/app/enum"
	"time"
)

type FiltersBoards struct {
	UserID        uint64 `json:"user_id"`
	MasterClassID uint64 `json:"master_class_id"`
	CurrentStatus string `json:"current_status"`
}

type UpdateBoardDTO struct {
	ID            uint               `json:"id"`
	CurrentStatus enum.CurrentStatus `json:"current_status,omitempty"`
	Note          string             `json:"note,omitempty"`
}

type CreateBoardDTO struct {
	MasterClassID uint               `json:"master_class_id"`
	CurrentStatus enum.CurrentStatus `json:"current_status"`
	Note          string             `json:"note,omitempty"`
}

type BoardDTO struct {
	ID            uint                 `json:"id"`
	UserID        uint                 `json:"user_id"`
	MasterClass   MasterClassNoDateDTO `json:"master_class"`
	CurrentStatus enum.CurrentStatus   `json:"current_status"`
	Note          string               `json:"note"`
	CreatedAt     time.Time            `json:"createdAt"`
	UpdatedAt     time.Time            `json:"updatedAt"`
}

type BoardsDTO struct {
	ID            uint               `json:"id"`
	UserID        uint               `json:"user_id"`
	MasterClassID uint               `json:"master_class"`
	CurrentStatus enum.CurrentStatus `json:"current_status"`
}

type NewBoardDTO struct {
	ID            uint               `json:"id"`
	UserID        uint               `json:"user_id"`
	MasterClassID uint               `json:"master_class"`
	CurrentStatus enum.CurrentStatus `json:"current_status"`
	Note          string             `json:"note"`
	CreatedAt     time.Time          `json:"createdAt"`
	UpdatedAt     time.Time          `json:"updatedAt"`
}
