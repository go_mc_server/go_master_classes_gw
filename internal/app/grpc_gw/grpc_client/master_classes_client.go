package grpc_client

import (
	"context"
	"go_master_classes_gw/internal/app/mistakes"
	"go_master_classes_gw/internal/app/utils"
	"log"
	"time"

	"google.golang.org/grpc"

	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"
)

var MasterClassesGrpcClient MasterClassesGRPCClientInterface

type MasterClassesGRPCClientInterface interface {
	GetMasterClasses(ctx context.Context, req *pb.FilterMasterClasses) (*pb.ListMasterClasses, error)
	GetMasterClassByID(ctx context.Context, req *pb.MasterClassID) (*pb.MasterClassDTO, error)
	CreateMasterClass(ctx context.Context, req *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error)
	UpdateUserCreateMC(ctx context.Context, req *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error)
	UpdateMasterClass(ctx context.Context, req *pb.UpdateMasterClassDTO) (*pb.NewMasterClassDTO, error)
	DeleteMasterClass(ctx context.Context, req *pb.UserMasterClassID) (*pb.Empty, error)
}

func SetMasterClassesGRPCClient(client MasterClassesGRPCClientInterface) {
	MasterClassesGrpcClient = client
}

type MasterClassesClient struct {
	client pb.MasterClassesClient
	conn   *grpc.ClientConn
}

func NewMasterClassesGRPCClient(conn *grpc.ClientConn) *MasterClassesClient {
	return &MasterClassesClient{
		client: pb.NewMasterClassesClient(conn),
		conn:   conn,
	}
}

func (g *MasterClassesClient) Close() {
	if g.conn != nil {
		if err := g.conn.Close(); err != nil {
			log.Printf("Failed to close gRPC connection MasterClassesClient: %v", err)
		}
	}
}

func (g *MasterClassesClient) GetMasterClasses(
	ctx context.Context, req *pb.FilterMasterClasses) (*pb.ListMasterClasses, error) {
	log.Printf("Received GetMasterClasses %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.GetMasterClasses(ctx, req)
	if err != nil {
		log.Printf("Error GetMasterClasses: %v", err)
		return nil, err
	}
	if res == nil || len(res.ListMasterClasses) == 0 {
		log.Printf("No content")
		return nil, mistakes.NoContent
	}

	log.Printf("Success GetMasterClasses: %v", res)
	return res, nil
}

func (g *MasterClassesClient) GetMasterClassByID(
	ctx context.Context, req *pb.MasterClassID) (*pb.MasterClassDTO, error) {
	log.Printf("Received GetMasterClassByID %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.GetMasterClassByID(ctx, req)
	if err != nil {
		log.Printf("Error GetMasterClassByID: %v", err)
		return nil, err
	}

	if res.Id == 0 {
		log.Printf("No content")
		return nil, mistakes.NoContent
	}

	log.Printf("Success GetMasterClassByID: %v", res)
	return res, nil
}

func (g *MasterClassesClient) CreateMasterClass(
	ctx context.Context, req *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	log.Printf("Received CreateMasterClass %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.CreateMasterClass(ctx, req)
	if err != nil {
		log.Printf("Error CreateMasterClass: %v", err)
		return nil, err
	}

	log.Printf("Success CreateMasterClass: %v", res)
	return res, nil

}

func (g *MasterClassesClient) UpdateUserCreateMC(
	ctx context.Context, req *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	log.Printf("Received UpdateUserCreateMC %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.UpdateUserCreateMC(ctx, req)
	if err != nil {
		log.Printf("Error UpdateUserCreateMC: %v", err)
		return nil, err
	}

	log.Printf("Success UpdateUserCreateMC: %v", res)
	return res, nil

}

func (g *MasterClassesClient) UpdateMasterClass(
	ctx context.Context, req *pb.UpdateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	log.Printf("Received UpdateMasterClass %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.UpdateMasterClass(ctx, req)
	if err != nil {
		log.Printf("Error UpdateMasterClass: %v", err)
		return nil, err
	}

	log.Printf("Success UpdateMasterClass: %v", res)
	return res, nil
}

func (g *MasterClassesClient) DeleteMasterClass(
	ctx context.Context, req *pb.UserMasterClassID) (*pb.Empty, error) {
	log.Printf("Received DeleteMasterClass %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.DeleteMasterClass(ctx, req)
	if err != nil {
		log.Printf("Error DeleteMasterClass: %v", err)
		return nil, err
	}

	log.Printf("Success DeleteMasterClass: %v", res)
	return res, nil
}

var _ MasterClassesGRPCClientInterface = (*MasterClassesClient)(nil)
