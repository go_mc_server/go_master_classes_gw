package grpc_client

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc"

	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/tex"
)

var TexGrpcClient TexGRPCClientInterface

type TexGRPCClientInterface interface {
	GetTexResponse(ctx context.Context, text string) (*pb.TexResponse, error)
}

// SetGRPCClient устанавливает grpcClient
func SetGRPCClient(client TexGRPCClientInterface) {
	TexGrpcClient = client
}

type GRPCTexClient struct {
	client pb.TexClient
	conn   *grpc.ClientConn
}

func NewTexGRPCClient(conn *grpc.ClientConn) *GRPCTexClient {
	return &GRPCTexClient{
		client: pb.NewTexClient(conn),
		conn:   conn,
	}
}

func (g *GRPCTexClient) Close() {
	if g.conn != nil {
		if err := g.conn.Close(); err != nil {
			log.Printf("Failed to close gRPC connection GRPCTexClient: %v", err)
		}
	}
}

func (g *GRPCTexClient) GetTexResponse(ctx context.Context, text string) (*pb.TexResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	/*
		ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
			if cancel != nil {
				defer cancel()
			}
	*/

	return g.client.GetTex(ctx, &pb.TexRequest{Text: text})
}
