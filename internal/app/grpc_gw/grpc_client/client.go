package grpc_client

import "google.golang.org/grpc"

var (
	grpcTexClient           GRPCTexClient
	grpcUsersClient         UsersClient
	grpcMasterClassesClient MasterClassesClient
	grpcBoardsClient        BoardsClient
)

func InitGRPCClients(conn *grpc.ClientConn) {
	//Tex
	grpcTexClient = *NewTexGRPCClient(conn)
	SetGRPCClient(&grpcTexClient)

	//Users
	grpcUsersClient = *NewUsersGRPCClient(conn)
	SetUsersGRPCClient(&grpcUsersClient)

	//MasterClasses
	grpcMasterClassesClient = *NewMasterClassesGRPCClient(conn)
	SetMasterClassesGRPCClient(&grpcMasterClassesClient)

	//Boards
	grpcBoardsClient = *NewBoardsGRPCClient(conn)
	SetBoardsGRPCClient(&grpcBoardsClient)
}

func CloseGRPCClients() {
	//Tex
	if &grpcTexClient != nil {
		grpcTexClient.Close()
	}

	//Users
	if &grpcUsersClient != nil {
		grpcUsersClient.Close()
	}

	//MasterClasses
	if &grpcMasterClassesClient != nil {
		grpcMasterClassesClient.Close()
	}

	//Boards
	if &grpcBoardsClient != nil {
		grpcBoardsClient.Close()
	}
}
