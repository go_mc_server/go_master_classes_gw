package grpc_client

import (
	"context"
	"go_master_classes_gw/internal/app/mistakes"
	"go_master_classes_gw/internal/app/utils"
	"log"
	"time"

	"google.golang.org/grpc"

	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
)

var BoardsGrpcClient BoardsGRPCClientInterface

type BoardsGRPCClientInterface interface {
	GetBoards(ctx context.Context, req *pb.FilterBoards) (*pb.ListBoards, error)
	GetBoardByID(ctx context.Context, req *pb.BoardID) (*pb.BoardDTO, error)
	CreateBoard(ctx context.Context, req *pb.CreateBoardDTO) (*pb.NewBoardDTO, error)
	UpdateBoard(ctx context.Context, req *pb.UpdateBoardDTO) (*pb.NewBoardDTO, error)
	DeleteBoard(ctx context.Context, req *pb.UserBoardID) (*pb.Empty, error)
}

func SetBoardsGRPCClient(client BoardsGRPCClientInterface) {
	BoardsGrpcClient = client
}

type BoardsClient struct {
	client pb.BoardsClient
	conn   *grpc.ClientConn
}

func NewBoardsGRPCClient(conn *grpc.ClientConn) *BoardsClient {
	return &BoardsClient{
		client: pb.NewBoardsClient(conn),
		conn:   conn,
	}
}

func (g *BoardsClient) Close() {
	if g.conn != nil {
		if err := g.conn.Close(); err != nil {
			log.Printf("Failed to close gRPC connection BoardsClient: %v", err)
		}
	}
}

func (g *BoardsClient) GetBoards(
	ctx context.Context, req *pb.FilterBoards) (*pb.ListBoards, error) {
	log.Printf("Received GetBoards %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.GetBoards(ctx, req)
	if err != nil {
		log.Printf("Error GetBoards: %v", err)
		return nil, err
	}

	if res == nil || len(res.ListBoards) == 0 {
		log.Printf("No content")
		return nil, mistakes.NoContent
	}

	log.Printf("Success GetBoards: %v", res)
	return res, nil
}

func (g *BoardsClient) GetBoardByID(
	ctx context.Context, req *pb.BoardID) (*pb.BoardDTO, error) {
	log.Printf("Received GetBoardByID %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.GetBoardByID(ctx, req)
	if err != nil {
		log.Printf("Error GetBoardByID: %v", err)
		return nil, err
	}

	if res.Id == 0 {
		log.Printf("No content")
		return nil, mistakes.NoContent
	}

	log.Printf("Success GetBoardByID: %v", res)
	return res, nil
}

func (g *BoardsClient) CreateBoard(
	ctx context.Context, req *pb.CreateBoardDTO) (*pb.NewBoardDTO, error) {
	log.Printf("Received CreateBoard %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.CreateBoard(ctx, req)
	if err != nil {
		log.Printf("Error CreateBoard: %v", err)
		return nil, err
	}

	log.Printf("Success CreateBoard: %v", res)
	return res, nil

}

func (g *BoardsClient) UpdateBoard(
	ctx context.Context, req *pb.UpdateBoardDTO) (*pb.NewBoardDTO, error) {
	log.Printf("Received UpdateBoard %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.UpdateBoard(ctx, req)
	if err != nil {
		log.Printf("Error UpdateBoard: %v", err)
		return nil, err
	}

	log.Printf("Success UpdateBoard: %v", res)
	return res, nil
}

func (g *BoardsClient) DeleteBoard(
	ctx context.Context, req *pb.UserBoardID) (*pb.Empty, error) {
	log.Printf("Received DeleteBoard %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.DeleteBoard(ctx, req)
	if err != nil {
		log.Printf("Error DeleteBoard: %v", err)
		return nil, err
	}

	log.Printf("Success DeleteBoard: %v", res)
	return res, nil
}
