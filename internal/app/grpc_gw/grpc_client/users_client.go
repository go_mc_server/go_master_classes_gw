package grpc_client

import (
	"context"
	"go_master_classes_gw/internal/app/mistakes"
	"log"
	"time"

	"google.golang.org/grpc"

	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
	"go_master_classes_gw/internal/app/utils"
)

var UsersGrpcClient UsersGRPCClientInterface

type UsersGRPCClientInterface interface {
	LoginUser(ctx context.Context, req *pb.UserLoginDetails) (*pb.UserDTO, error)
	GetUsers(ctx context.Context, req *pb.FilterUsers) (*pb.ListUsers, error)
	GetUserByID(ctx context.Context, req *pb.UserID) (*pb.UserDTO, error)
	CreateUser(ctx context.Context, req *pb.CreateUserDTO) (*pb.NewUserDTO, error)
	UpdateUser(ctx context.Context, req *pb.UpdateUserDTO) (*pb.NewUserDTO, error)
	UpdatePasswordUser(ctx context.Context, req *pb.UpdatePasswordDTO) (*pb.NewUserDTO, error)
	DeleteUser(ctx context.Context, req *pb.UserID) (*pb.Empty, error)
}

func SetUsersGRPCClient(client UsersGRPCClientInterface) {
	UsersGrpcClient = client
}

type UsersClient struct {
	client pb.UsersClient
	conn   *grpc.ClientConn
}

func NewUsersGRPCClient(conn *grpc.ClientConn) *UsersClient {
	return &UsersClient{
		client: pb.NewUsersClient(conn),
		conn:   conn,
	}
}

func (g *UsersClient) Close() {
	if g.conn != nil {
		if err := g.conn.Close(); err != nil {
			log.Printf("Failed to close gRPC connection UsersClient: %v", err)
		}
	}
}

func (g *UsersClient) LoginUser(
	ctx context.Context, req *pb.UserLoginDetails) (*pb.UserDTO, error) {
	log.Printf("Received LoginUser %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.LoginUser(ctx, req)
	if err != nil {
		log.Printf("Error LoginUser: %v", err)
		return nil, err
	}

	log.Printf("Success LoginUser: %v", res)

	return res, nil
}

func (g *UsersClient) GetUsers(
	ctx context.Context, req *pb.FilterUsers) (*pb.ListUsers, error) {
	log.Printf("Received GetUserByID %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.GetUsers(ctx, req)
	if err != nil {
		log.Printf("Error GetUserByID: %v", err)
		return nil, err
	}

	if res == nil || len(res.ListUsers) == 0 {
		log.Printf("No content")
		return nil, mistakes.NoContent
	}

	log.Printf("Success GetUserByID: %v", res)
	return res, nil
}

func (g *UsersClient) GetUserByID(
	ctx context.Context, req *pb.UserID) (*pb.UserDTO, error) {
	log.Printf("Received GetUserByID %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.GetUserByID(ctx, req)
	if err != nil {
		log.Printf("Error GetUserByID: %v", err)
		return nil, err
	}

	if res.Id == 0 {
		log.Printf("No content")
		return nil, mistakes.NoContent
	}

	log.Printf("Success GetUserByID: %v", res)
	return res, nil
}

func (g *UsersClient) CreateUser(
	ctx context.Context, req *pb.CreateUserDTO) (*pb.NewUserDTO, error) {
	log.Printf("Received CreateUser %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.CreateUser(ctx, req)
	if err != nil {
		log.Printf("Error CreateUser: %v", err)
		return nil, err
	}

	log.Printf("Success CreateUser: %v", res)
	return res, nil

}

func (g *UsersClient) UpdateUser(
	ctx context.Context, req *pb.UpdateUserDTO) (*pb.NewUserDTO, error) {
	log.Printf("Received UpdateUser %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.UpdateUser(ctx, req)
	if err != nil {
		log.Printf("Error UpdateUser: %v", err)
		return nil, err
	}

	log.Printf("Success UpdateUser: %v", res)
	return res, nil
}

func (g *UsersClient) UpdatePasswordUser(
	ctx context.Context, req *pb.UpdatePasswordDTO) (*pb.NewUserDTO, error) {
	log.Printf("Received UpdatePasswordUser %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.UpdatePasswordUser(ctx, req)
	if err != nil {
		log.Printf("Error UpdatePasswordUser: %v", err)
		return nil, err
	}
	log.Printf("Success UpdatePasswordUser: %v", res)
	return res, nil
}

func (g *UsersClient) DeleteUser(
	ctx context.Context, req *pb.UserID) (*pb.Empty, error) {
	log.Printf("Received DeleteUser %v", req)
	ctx, cancel := utils.EnsureTimeout(ctx, 5*time.Second)
	if cancel != nil {
		defer cancel()
	}

	res, err := g.client.DeleteUser(ctx, req)
	if err != nil {
		log.Printf("Error DeleteUser: %v", err)
		return nil, err
	}

	log.Printf("Success DeleteUser: %v", res)
	return res, nil
}
