package handlers

import (
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/grpc_gw/grpc_client"

	"net/http"
)

// GetTestHandler godoc
// @Summary Получение текста
// @Description Получает текст от gRPC сервиса
// @Tags текст
// @Accept  json
// @Produce  json
// @Param text path string true "Text для получения"
// @Router /tex/{text} [get]
func GetTestHandler(c *gin.Context) {

	text := c.Param("text")
	println(text)

	if text == "" {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	resp, err := grpc_client.TexGrpcClient.GetTexResponse(c.Request.Context(), text)
	if err != nil {
		println("gRPC error: ", err.Error())
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	c.JSON(http.StatusOK, resp)
}
