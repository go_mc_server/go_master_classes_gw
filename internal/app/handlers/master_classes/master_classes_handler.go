package master_classes

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"go_master_classes_gw/internal/app/services"
)

type HandlerMasterClasses struct {
	MasterClassesService *services.MasterClassesService
}

// GetMasterClasses godoc
// @Summary Список  мастер-классов
// @Description Возвращет список мастер-классов с фильтрацией
// @Tags master-classes
// @Accept  json
// @Produce  json
// @Param author_id query uint false "ID автора"
// @Param type_needlework query enum.TypeNeedlework false "Тип рукоделия"
// @Param name query string false "Название мастер-класса"
// @Param materials query string false "Используемые материалы"
// @Param is_active query bool false "Активный?"
// @Success 200 {array} []dto.MasterClassesDTO
// @Success 204 {string} string "No Content"
// @Router /master_classes [get]
func (h HandlerMasterClasses) GetMasterClasses(c *gin.Context) {
	filter := dto.FiltersMasterClasses{
		TypeNeedlework: c.Query("type_needlework"),
		Name:           c.Query("name"),
		Materials:      c.Query("materials"),
	}
	author := c.Query("author_id")
	if author != "" {
		authorID, err := strconv.ParseUint(author, 10, 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		filter.AuthorID = authorID
	}
	isActiveStr := c.Query("is_active")
	if isActiveStr != "" {
		isActive, err := strconv.ParseBool(isActiveStr)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		filter.IsActive = &isActive
	}

	masterClasses, err := h.MasterClassesService.GetAllMasterClasses(c.Request.Context(), &filter)
	if err != nil {
		if errors.Is(err, mistakes.NoContent) {
			c.JSON(http.StatusNoContent, masterClasses)
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, gin.H{"masterClasses": masterClasses})
}

// GetMasterClass godoc
// @Summary Мастер-класс по ID
// @Description Возвращет мастер-класс по ID
// @Tags master-classes
// @Accept  json
// @Produce  json
// @Param id path uint true "Master-class ID"
// @Success 200 {array} dto.MasterClassDTO
// @Success 204 {string} string "No Content"
// @Router /master_classes/{id} [get]
func (h HandlerMasterClasses) GetMasterClass(c *gin.Context) {
	masterClassId, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	masterClassDTO, err := h.MasterClassesService.GetMasterClassByID(c.Request.Context(), masterClassId)
	if err != nil {
		if errors.Is(err, mistakes.NoContent) {
			c.JSON(http.StatusNoContent, masterClassDTO)
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, masterClassDTO)
}

// CreateMasterClass godoc
// @Summary Создать мастер-класс
// @Description Возвращет данные о созданном мастер-классе
// @Tags master-classes
// @Accept  json
// @Produce  json
// @Param master-class body dto.CreateMasterClassDTO true "Данные мастер-класса"
// @Success 200 {array} dto.NewMasterClassesDTO
// @Router /master_classes [post]
func (h HandlerMasterClasses) CreateMasterClass(c *gin.Context) {
	idUser := c.MustGet("UsId").(uint)
	var masterClass *dto.CreateMasterClassDTO
	if err := c.ShouldBindJSON(&masterClass); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var newMasterClass *dto.NewMasterClassesDTO
	var err error
	role := c.GetString("Role")
	if role == "Admin" || role == "Master" {
		newMasterClass, err = h.MasterClassesService.CreateMasterClass(c.Request.Context(), uint64(idUser), masterClass)
		if err != nil {
			if errors.Is(err, mistakes.ErrConversion) {
				c.AbortWithStatusJSON(http.StatusBadRequest,
					gin.H{"message": "Новая доска пользователя успешно создана. Ошибка вывода созданных данных.",
						"error": err.Error()})
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
			return
		}
	} else if role == "ActiveUser" {
		newMasterClass, err = h.MasterClassesService.UpdateUserCreateMC(c.Request.Context(), uint64(idUser), masterClass)
		if err != nil {
			if errors.Is(err, mistakes.ErrConversion) {
				c.AbortWithStatusJSON(http.StatusBadRequest,
					gin.H{"message": "Новая доска пользователя успешно создана. Ошибка вывода созданных данных.",
						"error": err.Error()})
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
			return
		}
	} else {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Недостаточно прав на создание мастер-класса"})
		return
	}

	c.JSON(http.StatusOK,
		gin.H{"message": "Создан новый мастер-класс",
			"masterClass": newMasterClass})
}
