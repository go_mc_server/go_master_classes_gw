package master_classes

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"net/http"
	"strconv"
)

// GetMasterClassesMine godoc
// @Summary Список своих мастер-классов
// @Description Возвращет список своих мастер-классов с фильтрацией
// @Tags my-master-classes
// @Accept  json
// @Produce  json
// @Param type_needlework query enum.TypeNeedlework false "Тип рукоделия"
// @Param name query string false "Название мастер-класса"
// @Param materials query string false "Используемые материалы"
// @Param is_active query bool false "Активный?"
// @Success 200 {array} []dto.MasterClassesDTO
// @Success 204 {string} string "No Content"
// @Router /master_classes/mine [get]
func (h HandlerMasterClasses) GetMasterClassesMine(c *gin.Context) {
	userID := c.MustGet("UsId").(uint)
	filter := dto.FiltersMasterClasses{
		AuthorID:       uint64(userID),
		TypeNeedlework: c.Query("type_needlework"),
		Name:           c.Query("name"),
		Materials:      c.Query("materials"),
	}

	isActiveStr, exists := c.GetQuery("is_active")
	if exists {
		isActive, err := strconv.ParseBool(isActiveStr)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		filter.IsActive = &isActive
	}

	masterClasses, err := h.MasterClassesService.GetAllMasterClasses(c.Request.Context(), &filter)
	if err != nil {
		if errors.Is(err, mistakes.NoContent) {
			c.JSON(http.StatusNoContent, masterClasses)
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, gin.H{"masterClasses": masterClasses})
}

// UpdateMasterClassMine godoc
// @Summary Изменить мастер-класс
// @Description Изменяет данные мастер-класса текущего пользователя по его ID
// @Tags my-master-classes
// @Accept  json
// @Produce  json
// @Param user body dto.UpdateMasterClassDTO true "Master-class"
// @Success 200 {object} dto.NewMasterClassesDTO
// @Router /master_classes/mine [patch]
func (h HandlerMasterClasses) UpdateMasterClassMine(c *gin.Context) {
	idUser := c.MustGet("UsId").(uint)
	var masterClass dto.UpdateMasterClassDTO
	if err := c.ShouldBindJSON(&masterClass); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	updateMC, err := h.MasterClassesService.UpdateMasterClass(c.Request.Context(), uint64(idUser), &masterClass)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, updateMC)
}

// DeleteMasterClassMine godoc
// @Summary Удалить мастер-класс
// @Description Удаляет мастер-класс пользователя по его ID
// @Tags my-master-classes
// @Param id path uint true "Master-class ID"
// @Success 204 "No content"
// @Router /master_classes/mine/{id} [delete]
func (h HandlerMasterClasses) DeleteMasterClassMine(c *gin.Context) {
	userID := c.MustGet("UsId").(uint)
	idMC := c.Param("id")
	masterClassIDUint, err := strconv.ParseUint(idMC, 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	err = h.MasterClassesService.DeleteMasterClass(c.Request.Context(), uint64(userID), masterClassIDUint)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	c.Status(http.StatusNoContent)
}
