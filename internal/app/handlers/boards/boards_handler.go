package boards

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"go_master_classes_gw/internal/app/services"
	"net/http"
	"strconv"
)

type HandlerBoards struct {
	BoardsService *services.BoardsService
}

// GetBoardsByID godoc
// @Summary Доска пользователя по ID
// @Description Получает данные доски пользователя по ID
// @Tags boards
// @Accept  json
// @Produce  json
// @Param id path uint true "Board ID"
// @Success 200 {object} dto.BoardDTO
// @Success 204 {string} string "No Content"
// @Router /boards/{id} [get]
func (h HandlerBoards) GetBoardsByID(c *gin.Context) {
	boardIDUint, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	board, err := h.BoardsService.GetBoardByID(c.Request.Context(), boardIDUint)
	if err != nil {
		if errors.Is(err, mistakes.NoContent) {
			c.JSON(http.StatusNoContent, board)
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, board)

}

// CreateBoard godoc
// @Summary Создать доску пользователя
// @Description Создать доску текущего пользователя по его ID
// @Tags boards
// @Accept  json
// @Produce  json
// @Param user body dto.CreateBoardDTO true "Board"
// @Success 200 {object} dto.NewBoardDTO
// @Router /boards [post]
func (h HandlerBoards) CreateBoard(c *gin.Context) {
	idUser := c.MustGet("UsId").(uint)
	var board *dto.CreateBoardDTO
	if err := c.ShouldBindJSON(&board); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newBoard, err := h.BoardsService.CreateBoard(c.Request.Context(), uint64(idUser), board)
	if err != nil {
		if errors.Is(err, mistakes.ErrConversion) {
			c.AbortWithStatusJSON(http.StatusBadRequest,
				gin.H{"message": "Новая доска пользователя успешно создана. Ошибка вывода созданных данных.",
					"error": err.Error()})
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, newBoard)
}
