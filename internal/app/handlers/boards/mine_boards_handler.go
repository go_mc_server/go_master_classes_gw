package boards

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"net/http"
	"strconv"
)

// GetBoardsMine godoc
// @Summary Пользовательские доски c фильтрацией
// @Description Доски текущего пользователя по его ID
// @Tags my-boards
// @Accept  json
// @Produce  json
// @Param current_status query enum.CurrentStatus false "Текущий статус"
// @Success 200 {object} []dto.BoardsDTO
// @Success 204 {string} string "No Content"
// @Router /boards/mine [get]
func (h HandlerBoards) GetBoardsMine(c *gin.Context) {
	idUser := c.MustGet("UsId").(uint)
	filter := dto.FiltersBoards{
		UserID:        uint64(idUser),
		CurrentStatus: c.Query("current_status"),
	}

	boards, err := h.BoardsService.GetAllBoards(c.Request.Context(), &filter)
	if err != nil {
		if errors.Is(err, mistakes.NoContent) {
			c.JSON(http.StatusNoContent, boards)
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, boards)
}

// UpdateBoardMine godoc
// @Summary Изменить доску пользователя
// @Description Изменяет доску текущего пользователя по его ID
// @Tags my-boards
// @Accept  json
// @Produce  json
// @Param user body dto.UpdateBoardDTO true "Board"
// @Success 200 {object} dto.NewBoardDTO
// @Router /boards/mine [patch]
func (h HandlerBoards) UpdateBoardMine(c *gin.Context) {
	idUser := c.MustGet("UsId").(uint)
	var board dto.UpdateBoardDTO
	if err := c.ShouldBindJSON(&board); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	updateBoard, err := h.BoardsService.UpdateBoard(c.Request.Context(), uint64(idUser), &board)
	if err != nil {
		if errors.Is(err, mistakes.ErrConversion) {
			c.AbortWithStatusJSON(http.StatusBadRequest,
				gin.H{"message": "Доска успешно обновлена. Ошибка вывода созданных данных.",
					"error": err.Error()})
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, updateBoard)

}

// DeleteBoardMine godoc
// @Summary Удалить доску
// @Description Удаляет доску пользователя по его ID
// @Tags my-boards
// @Param id path uint true "Board ID"
// @Success 204 "No content"
// @Router /boards/mine/{id} [delete]
func (h HandlerBoards) DeleteBoardMine(c *gin.Context) {
	userID := c.MustGet("UsId").(uint)
	boardIDUint, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = h.BoardsService.DeleteBoard(c.Request.Context(), uint64(userID), boardIDUint)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	c.Status(http.StatusNoContent)
}
