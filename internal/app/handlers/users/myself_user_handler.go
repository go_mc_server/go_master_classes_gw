package users

import (
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/dto"
	"net/http"
)

// GetMyself godoc
// @Summary Получение данных о себе
// @Description Получение данных о себе
// @Tags user
// @Accept  json
// @Produce  json
// @Success 200 {object} dto.UserDTO
// @Router /user/myself [get]
func (h HandlerUsers) GetMyself(c *gin.Context) {
	IDUser := c.MustGet("UsId").(uint)
	user, err := h.UserService.GetUserByID(c.Request.Context(), uint64(IDUser))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, user)
}

// UpdateMyself godoc
// @Summary Обновить пользователя
// @Description Изменяет данные пользователя по ID
// @Tags user
// @Accept  json
// @Produce  json
// @Param user body dto.UpdateMyselfDTO true "User"
// @Success 200 {object} dto.NewUserDTO
// @Router /user/myself [patch]
func (h HandlerUsers) UpdateMyself(c *gin.Context) {
	var user dto.UpdateUserDTO
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	userID := c.MustGet("UsId").(uint)
	user.ID = uint64(userID)

	newUser, err := h.UserService.UpdateUser(c.Request.Context(), &user)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	c.JSON(http.StatusOK, newUser)
}

// UpdatePassword godoc
// @Summary Сменить пароль
// @Description Изменяет пароль пользователя
// @Tags auth
// @Accept  json
// @Produce  json
// @Param user body dto.UpdatePasswordDTO true "User"
// @Success 200 {object} dto.NewUserDTO
// @Router /password [patch]
func (h HandlerUsers) UpdatePassword(c *gin.Context) {
	var user *dto.UpdatePasswordDTO
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newUser, err := h.UserService.UpdatePassword(c.Request.Context(), user)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	c.JSON(http.StatusCreated, newUser)
}
