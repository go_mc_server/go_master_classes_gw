package users

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/auth"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"log"
	"net/http"
)

// LoginUser godoc
// @Summary Аутентификация пользователя
// @Description Аудентификация пользователя по email и password
// @Tags auth
// @Accept  json
// @Produce  json
// @Param auth body dto.UserLoginDetails true "Данные пользователя"
// @Success 200 {object} dto.UserDTO
// @Router /login [post]
func (h HandlerUsers) LoginUser(c *gin.Context) {
	var authUser *dto.UserLoginDetails
	if err := c.ShouldBindJSON(&authUser); err != nil {
		log.Printf("UsersHandler.LoginUser: %v", err.Error())
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userDTO, err := h.UserService.GetUserByEmail(c.Request.Context(), authUser)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	accessToken, refreshToken, err := auth.GenerateAccessRefreshTokens(&userDTO.ID, &userDTO.Role)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest,
			gin.H{
				"error":   err.Error(),
				"message": "Не удалось создать accessToken и refresh token",
			})
		return
	}
	// Установка cookie
	http.SetCookie(c.Writer, auth.CreateAccessTokenCookie(accessToken))
	http.SetCookie(c.Writer, auth.CreateRefreshTokenCookie(refreshToken))

	c.JSON(http.StatusOK, gin.H{
		"message":      "Успешный вход",
		"user":         userDTO,
		"accessToken":  accessToken,
		"refreshToken": refreshToken,
	})
}

// LogoutUser логин пользователя
// @Summary Logout user
// @Description Выход пользователя
// @Tags auth
// @Accept  x-www-form-urlencoded
// @Produce  json
// @Success 200 {object} map[string]interface{}
// @Failure 400 {object} map[string]interface{}
// @Router /logout [post]
func (h HandlerUsers) LogoutUser(c *gin.Context) {
	// Очистка куки
	c.SetCookie("access_token", "", -1, "/", "localhost", false, true)
	c.SetCookie("refresh_token", "", -1, "/", "localhost", false, true)

	c.JSON(http.StatusOK, gin.H{"message": "Выход выполнен"})
}

// CreateUser godoc
// @Summary Создать пользователя
// @Description Создает нового пользователя
// @Tags auth
// @Accept  json
// @Produce  json
// @Param user body dto.CreateUserDTO true "Данные нового пользователя"
// @Success 201 {object} dto.UserCreationResponseDTO
// @Router /register [post]
func (h HandlerUsers) CreateUser(c *gin.Context) {
	var user *dto.CreateUserDTO
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newUserDTO, err := h.UserService.CreateUser(c.Request.Context(), user)
	if err != nil {
		if errors.Is(err, mistakes.ErrConversion) {
			c.AbortWithStatusJSON(http.StatusBadRequest,
				gin.H{"message": "Новый пользователь успешно создан. Ошибка вывода созданных данных.",
					"error": err.Error()})
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	role := c.GetString("Role")
	if role == "Admin" {
		c.JSON(http.StatusCreated, gin.H{
			"message": "Пользователь успешно создан Администратором",
			"user":    newUserDTO,
		})
		return
	}

	accessToken, refreshToken, err := auth.GenerateAccessRefreshTokens(&newUserDTO.ID, &newUserDTO.Role)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest,
			gin.H{
				"error":   err.Error(),
				"message": "Не удалось создать accessToken и refresh token для нового пользователя",
				"user":    newUserDTO,
			})
		return
	}
	// Установка cookie
	http.SetCookie(c.Writer, auth.CreateAccessTokenCookie(accessToken))
	http.SetCookie(c.Writer, auth.CreateRefreshTokenCookie(refreshToken))

	c.JSON(http.StatusCreated, gin.H{
		"message":      "Вы успешно зарегистрированы, доступ в лк открыт",
		"user":         newUserDTO,
		"accessToken":  accessToken,
		"refreshToken": refreshToken,
	})
}

// Перенаправление запроса
/*
func Redirect(c *gin.Context) {
	role := c.MustGet("Role").(string)
	switch role {
	case "Admin":
		c.Redirect(http.StatusMovedPermanently, "/admin")
	case "ActiveUser":
		c.Redirect(http.StatusMovedPermanently, "/active_user")
	case "Master":
		c.Redirect(http.StatusMovedPermanently, "/master")
	case "Blocked":
		c.JSON(http.StatusForbidden, gin.H{"status": "Forbidden"})
	}
}
*/
