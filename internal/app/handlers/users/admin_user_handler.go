// Обработчики запросов
package users

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/mistakes"
	"go_master_classes_gw/internal/app/services"
	"net/http"
	"strconv"
)

type HandlerUsers struct {
	UserService *services.UsersService
}

// GetUsers godoc
// @Summary Получить список всех пользователей
// @Description Возвращает список всех пользователей в системе
// @Tags users
// @Accept  json
// @Produce  json
// @Param LastName query string false "Фамилия"
// @Param FirstName query string false "Имя"
// @Param MiddleName query string false "Отчество"
// @Param Email query string false "Email"
// @Param Role query enum.RolesUser false "Роль"
// @Success 200 {array} []dto.UsersDTO
// @Success 204 {string} string "No Content"
// @Router /users [get]
func (h HandlerUsers) GetUsers(c *gin.Context) {
	role := c.MustGet("Role").(string)
	if role != "Admin" {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"message": "Недостаточно прав доступа"})
		return
	}

	filter := dto.FiltersUsers{
		LastName:   c.Query("LastName"),
		FirstName:  c.Query("FirstName"),
		MiddleName: c.Query("MiddleName"),
		Email:      c.Query("Email"),
		Role:       c.Query("Role"),
	}

	users, err := h.UserService.GetAllUsers(c.Request.Context(), &filter)
	if err != nil {
		if errors.Is(err, mistakes.NoContent) {
			c.JSON(http.StatusNoContent, users)
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, users)
}

// GetUserByID godoc
// @Summary Пользователь по ID
// @Description Получение пользователя по ID
// @Tags users
// @Accept  json
// @Produce  json
// @Param id path uint true "User ID"
// @Success 200 {object} dto.UserDTO
// @Success 204 {string} string "No Content"
// @Router /users/{id} [get]
func (h HandlerUsers) GetUserByID(c *gin.Context) {
	role := c.MustGet("Role").(string)
	if role != "Admin" {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"message": "Недостаточно прав доступа"})
		return
	}
	userIDUint, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user, err := h.UserService.GetUserByID(c.Request.Context(), userIDUint)
	if err != nil {
		if errors.Is(err, mistakes.NoContent) {
			c.JSON(http.StatusNoContent, user)
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		return
	}

	c.JSON(http.StatusOK, user)
}

// UpdateUserRole godoc
// @Summary Обновить роль пользователя
// @Description Обновляет роль пользователя по ID
// @Tags users
// @Accept  json
// @Produce  json
// @Param id query uint true "User ID"
// @Param role query enum.RolesUser true "Роль пользователя"
// @Success 200 {object} dto.NewUserDTO
// @Router /users/user_role [patch]
func (h HandlerUsers) UpdateUserRole(c *gin.Context) {
	role := c.MustGet("Role").(string)
	if role != "Admin" {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"message": "Недостаточно прав доступа"})
		return
	}
	userID := c.Query("id")
	userIDUint, err := strconv.ParseUint(userID, 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	userRole := c.Query("role")

	user := dto.UpdateUserDTO{ID: userIDUint, Role: userRole}
	newUser, err := h.UserService.UpdateUser(c.Request.Context(), &user)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	c.JSON(http.StatusCreated, newUser)
}

// DeleteUser godoc
// @Summary Удалить пользователя
// @Description Удаляет пользователя по ID
// @Tags users
// @Param id path uint true "User ID"
// @Success 204 "No content"
// @Router /users/{id} [delete]
func (h HandlerUsers) DeleteUser(c *gin.Context) {
	role := c.MustGet("Role").(string)
	if role != "Admin" {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"message": "Недостаточно прав доступа"})
		return
	}
	id := c.Param("id")
	userIDUint, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	err = h.UserService.DeleteUser(c.Request.Context(), userIDUint)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusNoContent)
}
