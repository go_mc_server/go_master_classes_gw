package logs

import (
	"github.com/sirupsen/logrus"
	"io"
	"os"
)

func SetupLogrus() *logrus.Logger {
	logger := logrus.New()

	// Включение цветов для терминала
	// Настройка вывод в консоль с цветами
	logger.SetFormatter(&logrus.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	})

	// Настройка вывода в файл
	file, err := os.OpenFile("app.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		mw := io.MultiWriter(os.Stdout, file)
		logger.SetOutput(mw)
	} else {
		// Если открытие файла провалилось, логируем ошибку и продолжаем с выводом в консоль
		logger.Warning("Не удалось войти в файл с использованием стандартного параметра stderr по умолчанию")
		logger.SetOutput(os.Stdout)
	}

	// Настройка уровней логирования
	logger.SetLevel(logrus.InfoLevel)

	return logger
}
