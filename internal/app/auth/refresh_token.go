package auth

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"go_master_classes_gw/internal/app/config"
	"net/http"
	"time"
)

// CreateRefreshTokens создание нового refresh_token.
func CreateRefreshTokens(userID *uint) (string, error) {
	// Генерация Refresh Token
	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": userID,
		"exp": time.Now().Add(time.Hour * 24 * 7).Unix(), // Refresh Token истекает через 1 неделю
		"jti": "unique-refresh-token-id",
	})

	refreshTokenString, err := refreshToken.SignedString([]byte(config.Cfg.RefreshSecretKey))
	if err != nil {
		return "", err
	}

	return refreshTokenString, nil
}

// VerifyRefreshToken верификация refresh_token и получение id пользователя.
func VerifyRefreshToken(refreshToken string) (uint, error) {
	token, err := jwt.Parse(
		refreshToken,
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(config.Cfg.RefreshSecretKey), nil
		})
	if err != nil {
		return 0, err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if jti, ok := claims["jti"].(string); ok {
			if jti == "unique-refresh-token-id" {
				userID, ok := claims["sub"].(float64)
				if !ok {
					return 0, errors.New("invalid sub claim")
				}
				return uint(userID), nil
			} else {
				return 0, errors.New("invalid jti claim")
			}
		} else {
			return 0, errors.New("jti claim is missing or not a string")
		}
	} else {
		return 0, errors.New("invalid token")
	}
}

// CreateRefreshTokenCookie создает куки для refresh_token.
func CreateRefreshTokenCookie(refreshToken string) *http.Cookie {
	expiration := time.Now().Add(7 * 24 * time.Hour)
	return &http.Cookie{
		Name:     "refresh_token",
		Value:    refreshToken,
		Expires:  expiration,
		Path:     "/",
		Domain:   "localhost",
		Secure:   false,
		HttpOnly: true,
	}
}
