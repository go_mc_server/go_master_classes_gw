package auth

import (
	"errors"
	"fmt"
	"go_master_classes_gw/internal/app/enum"
)

var (
	ErrCreateAccessToken  = errors.New("failed to create access token")
	ErrCreateRefreshToken = errors.New("failed to create refresh token")
)

func GenerateAccessRefreshTokens(userID *uint, userRole *enum.RolesUser) (string, string, error) {
	accessToken, err := CreateAccessTokens(userID, userRole)
	if err != nil {
		return "", "", fmt.Errorf("%w: %s", ErrCreateAccessToken, err)
	}

	refreshToken, err := CreateRefreshTokens(userID)
	if err != nil {
		return "", "", fmt.Errorf("%w: %s", ErrCreateRefreshToken, err)
	}

	return accessToken, refreshToken, nil
}

func GenerateAccessTokens(userID *uint, userRole *enum.RolesUser) (string, error) {
	accessToken, err := CreateAccessTokens(userID, userRole)
	if err != nil {
		return "", fmt.Errorf("%w: %s", ErrCreateAccessToken, err)
	}

	return accessToken, nil
}
