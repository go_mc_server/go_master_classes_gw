package auth

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"go_master_classes_gw/internal/app/config"
	"go_master_classes_gw/internal/app/enum"
	"net/http"
	"time"
)

// CreateAccessTokens создание нового access_token.
func CreateAccessTokens(userID *uint, userRole *enum.RolesUser) (string, error) {
	// Генерация Access Token
	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":  userID,
		"role": userRole,
		"exp":  time.Now().Add(time.Minute * 15).Unix(), // Access Token истекает через 15 минут
		"jti":  "unique-token-id",
	})

	accessTokenString, err := accessToken.SignedString([]byte(config.Cfg.AccessSecretKey))
	if err != nil {
		return "", err
	}

	return accessTokenString, nil
}

// VerifyAccessToken верификация accessToken, получение id и роли пользователя.
func VerifyAccessToken(accessToken string) (uint, string, error) {
	token, err := jwt.Parse(
		accessToken,
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(config.Cfg.AccessSecretKey), nil
		})
	if err != nil {
		return 0, "", err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if jti, ok := claims["jti"].(string); ok {
			if jti == "unique-token-id" {
				userID, ok := claims["sub"].(float64)
				if !ok {
					return 0, "", errors.New("invalid sub claim")
				}
				role, ok := claims["role"].(string)
				if !ok {
					return 0, "", errors.New("invalid sub claim")
				}
				return uint(userID), role, nil
			} else {
				return 0, "", errors.New("invalid jti claim")
			}
		} else {
			return 0, "", errors.New("jti claim is missing or not a string")
		}
	} else {
		return 0, "", errors.New("invalid token")
	}

}

// CreateAccessTokenCookie создает куки для access_token.
func CreateAccessTokenCookie(accessToken string) *http.Cookie {
	expiration := time.Now().Add(15 * time.Minute)
	return &http.Cookie{
		Name:     "access_token",
		Value:    accessToken,
		Expires:  expiration,
		Path:     "/",
		Domain:   "localhost",
		Secure:   false,
		HttpOnly: true,
	}
}
