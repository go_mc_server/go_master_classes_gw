package enum

type CurrentStatus string

const (
	Idea       CurrentStatus = "Idea"       //идея
	ToDo       CurrentStatus = "ToDo"       //сделать
	InProgress CurrentStatus = "InProgress" //в процессе
	Done       CurrentStatus = "Done"       // готово
	Archived   CurrentStatus = "Archived"   //архив
)
