package enum

type TypeNeedlework string

const (
	Crocheting          TypeNeedlework = "Crocheting"          //вязание крючком
	KnittingWithNeedles TypeNeedlework = "KnittingWithNeedles" //вязание спицами
	Macrame             TypeNeedlework = "Macrame"             //макраме
	Embroidery          TypeNeedlework = "Embroidery"          //вышивание
	Sewing              TypeNeedlework = "Sewing"              //шитьё
	Painting            TypeNeedlework = "Painting"            //рисование
	Felting             TypeNeedlework = "Felting"             //валяние
	Modeling            TypeNeedlework = "Modeling"            //лепка
)
