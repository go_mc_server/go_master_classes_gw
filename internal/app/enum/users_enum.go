package enum

type RolesUser string

const (
	Admin      RolesUser = "Admin"
	ActiveUser RolesUser = "ActiveUser"
	Master     RolesUser = "Master"
	Blocked    RolesUser = "Blocked"
)
