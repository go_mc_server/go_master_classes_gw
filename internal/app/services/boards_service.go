package services

import (
	"context"
	"github.com/mitchellh/mapstructure"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
	"go_master_classes_gw/internal/app/converters"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/grpc_gw/grpc_client"
	"go_master_classes_gw/internal/app/mistakes"
	"log"
)

type BoardsService struct{}

func (s *BoardsService) GetAllBoards(
	ctx context.Context, filterBoard *dto.FiltersBoards) ([]*dto.BoardsDTO, error) {
	var filter *pb.FilterBoards
	if err := mapstructure.Decode(filterBoard, &filter); err != nil {
		log.Printf("BoardsService.GetAllBoards conversion error: %v", err)
		return nil, err
	}

	boards, err := grpc_client.BoardsGrpcClient.GetBoards(ctx, filter)
	if err != nil {
		log.Printf("BoardsService.GetAllBoards error: %v", err)
	}

	if boards == nil {
		log.Printf("BoardsService.GetAllBoards response is nil")
		return nil, mistakes.NoContent
	}

	return converters.ConverterInListBoardsDTO(boards.ListBoards)
}

func (s *BoardsService) GetBoardByID(
	ctx context.Context, id uint64) (*dto.BoardDTO, error) {
	board, err := grpc_client.BoardsGrpcClient.GetBoardByID(ctx, &pb.BoardID{Id: id})
	if err != nil {
		log.Printf("BoardsService.GetBoardByID error1: %v", err)
		return nil, err
	}

	return converters.ConverterInBoardDTO(board)
}

func (s *BoardsService) CreateBoard(
	ctx context.Context, userID uint64, board *dto.CreateBoardDTO) (*dto.NewBoardDTO, error) {
	var createBoard *pb.CreateBoardDTO
	if err := mapstructure.Decode(board, &createBoard); err != nil {
		log.Printf("BoardsService.CreateBoard mapstructure error: %v", err)
		return nil, err
	}
	createBoard.UserId = userID

	newBoard, err := grpc_client.BoardsGrpcClient.CreateBoard(ctx, createBoard)
	if err != nil {
		log.Printf("BoardsService.CreateBoard error1: %v", err)
		return nil, err
	}

	return converters.ConverterInNewBoardDTO(newBoard)
}

func (s *BoardsService) UpdateBoard(
	ctx context.Context, userID uint64, updateBoard *dto.UpdateBoardDTO) (*dto.NewBoardDTO, error) {
	var updBoard *pb.UpdateBoardDTO
	if err := mapstructure.Decode(updateBoard, &updBoard); err != nil {
		log.Printf("BoardsService.UpdateBoard mapstructure error: %v", err)
		return nil, err
	}
	updBoard.UserId = userID

	board, err := grpc_client.BoardsGrpcClient.UpdateBoard(ctx, updBoard)
	if err != nil {
		log.Printf("BoardsService.UpdateBoard error1: %v", err)
		return nil, err
	}

	return converters.ConverterInNewBoardDTO(board)
}

func (s *BoardsService) DeleteBoard(
	ctx context.Context, userID, boardID uint64) error {
	deleteBoard := &pb.UserBoardID{UserId: userID, BoardId: boardID}
	_, err := grpc_client.BoardsGrpcClient.DeleteBoard(ctx, deleteBoard)
	if err != nil {
		return err
	}

	return nil
}
