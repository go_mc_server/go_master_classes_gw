package services

import (
	"context"
	"github.com/mitchellh/mapstructure"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
	"go_master_classes_gw/internal/app/converters"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/grpc_gw/grpc_client"
	"log"
)

type UsersService struct{}

func (s *UsersService) GetAllUsers(
	ctx context.Context, filterUser *dto.FiltersUsers) ([]*dto.UsersDTO, error) {
	var filter *pb.FilterUsers
	if err := mapstructure.Decode(filterUser, &filter); err != nil {
		log.Printf("UsersService.GetAllUsers conversion err: %v", err)
		return nil, err
	}

	users, err := grpc_client.UsersGrpcClient.GetUsers(ctx, filter)
	if err != nil {
		log.Printf("UsersService.GetAllUsers err: %v", err)
		return nil, err
	}

	return converters.ConverterInListUsersDTO(users.ListUsers)
}

func (s *UsersService) GetUserByID(
	ctx context.Context, id uint64) (*dto.UserDTO, error) {
	user, err := grpc_client.UsersGrpcClient.GetUserByID(ctx, &pb.UserID{Id: id})
	if err != nil {
		log.Printf("UsersService.GetUserByID err: %v", err)
		return nil, err
	}

	return converters.ConverterInUserDTO(user)
}

func (s *UsersService) GetUserByEmail(
	ctx context.Context, auth *dto.UserLoginDetails) (*dto.UserDTO, error) {
	var authUser *pb.UserLoginDetails
	if err := mapstructure.Decode(auth, &authUser); err != nil {
		log.Printf("UsersService.GetUserByEmail conversion error: %v", err)
		return nil, err
	}

	user, err := grpc_client.UsersGrpcClient.LoginUser(ctx, authUser)
	if err != nil {
		log.Printf("UsersService.GetUserByEmail err: %v", err)
		return nil, err
	}

	return converters.ConverterInUserDTO(user)
}

func (s *UsersService) CreateUser(
	ctx context.Context, user *dto.CreateUserDTO) (*dto.NewUserDTO, error) {
	var userPB *pb.CreateUserDTO
	if err := mapstructure.Decode(user, &userPB); err != nil {
		log.Printf("UsersService.CreateUser conversion err: %v", err)
		return nil, err
	}

	newUser, err := grpc_client.UsersGrpcClient.CreateUser(ctx, userPB)
	if err != nil {
		log.Printf("UsersService.CreateUser err: %v", err)
		return nil, err
	}

	return converters.ConverterInNewUserDTO(newUser)
}

func (s *UsersService) UpdateUser(
	ctx context.Context, user *dto.UpdateUserDTO) (*dto.NewUserDTO, error) {
	var usersDTO *pb.UpdateUserDTO
	if err := mapstructure.Decode(user, &usersDTO); err != nil {
		log.Printf("UsersService.UpdateUser conversion error: %v", err)
		return nil, err
	}

	newUser, err := grpc_client.UsersGrpcClient.UpdateUser(ctx, usersDTO)
	if err != nil {
		log.Printf("UsersService.UpdateUser err: %v", err)
	}

	return converters.ConverterInNewUserDTO(newUser)
}

func (s *UsersService) UpdatePassword(
	ctx context.Context, user *dto.UpdatePasswordDTO) (*dto.NewUserDTO, error) {
	var userUpdate *pb.UpdatePasswordDTO
	if err := mapstructure.Decode(user, &userUpdate); err != nil {
		log.Printf("UsersService.UpdatePassword conversion error: %v", err)
		return nil, err
	}

	newUser, err := grpc_client.UsersGrpcClient.UpdatePasswordUser(ctx, userUpdate)
	if err != nil {
		log.Printf("UsersService.UpdatePassword err: %v", err)
	}

	return converters.ConverterInNewUserDTO(newUser)
}

func (s *UsersService) DeleteUser(
	ctx context.Context, id uint64) error {
	_, err := grpc_client.UsersGrpcClient.DeleteUser(ctx, &pb.UserID{Id: id})
	if err != nil {
		log.Printf("UsersService.DeleteUser error: %v", err)
		return err
	}
	return nil
}
