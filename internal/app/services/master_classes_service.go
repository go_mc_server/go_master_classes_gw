package services

import (
	"context"
	"log"

	"github.com/mitchellh/mapstructure"

	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"

	"go_master_classes_gw/internal/app/converters"
	"go_master_classes_gw/internal/app/dto"
	"go_master_classes_gw/internal/app/grpc_gw/grpc_client"
)

type MasterClassesService struct{}

func (s *MasterClassesService) GetAllMasterClasses(
	ctx context.Context, filterMC *dto.FiltersMasterClasses) ([]*dto.MasterClassesDTO, error) {
	var filter *pb.FilterMasterClasses
	if err := mapstructure.Decode(filterMC, &filter); err != nil {
		log.Printf("MasterClassesService.GetAllMasterClasses conversion error: %v", err)
		return nil, err
	}

	masterClasses, err := grpc_client.MasterClassesGrpcClient.GetMasterClasses(ctx, filter)
	if err != nil {
		log.Printf("MasterClassesService.GetAllMasterClasses error: %v", err)
		return nil, err
	}

	return converters.ConverterInListMasterClassesDTO(masterClasses.ListMasterClasses)
}

func (s *MasterClassesService) GetMasterClassByID(
	ctx context.Context, id uint64) (*dto.MasterClassDTO, error) {
	masterClassPB := pb.MasterClassID{Id: id}
	masterClass, err := grpc_client.MasterClassesGrpcClient.GetMasterClassByID(ctx, &masterClassPB)
	if err != nil {
		log.Printf("MasterClassesService.GetMasterClassByID error1: %v", err)
		return nil, err
	}

	return converters.ConverterInMasterClassDTO(masterClass)
}

func (s *MasterClassesService) CreateMasterClass(
	ctx context.Context, userID uint64, masterClass *dto.CreateMasterClassDTO) (*dto.NewMasterClassesDTO, error) {
	var createMC *pb.CreateMasterClassDTO
	if err := mapstructure.Decode(masterClass, &createMC); err != nil {
		log.Printf("MasterClassesService.CreateMasterClass conversion error: %v", err)
		return nil, err
	}
	createMC.AuthorId = userID

	newMasterClass, err := grpc_client.MasterClassesGrpcClient.CreateMasterClass(ctx, createMC)
	if err != nil {
		log.Printf("MasterClassesService.CreateMasterClass error: %v", err)
		return nil, err
	}

	return converters.ConverterInNewMasterClassesDTO(newMasterClass)
}

func (s *MasterClassesService) UpdateUserCreateMC(
	ctx context.Context, userID uint64, masterClass *dto.CreateMasterClassDTO) (*dto.NewMasterClassesDTO, error) {
	var createMC *pb.CreateMasterClassDTO
	if err := mapstructure.Decode(masterClass, &createMC); err != nil {
		log.Printf("MasterClassesService.CreateMasterClass conversion error: %v", err)
		return nil, err
	}
	createMC.AuthorId = userID

	newMasterClass, err := grpc_client.MasterClassesGrpcClient.UpdateUserCreateMC(ctx, createMC)
	if err != nil {
		log.Printf("MasterClassesService.UpdateUserCreateMC error1: %v", err)
		return nil, err
	}

	return converters.ConverterInNewMasterClassesDTO(newMasterClass)
}

func (s *MasterClassesService) UpdateMasterClass(
	ctx context.Context, userID uint64, updateMC *dto.UpdateMasterClassDTO) (*dto.NewMasterClassesDTO, error) {
	var updMasterClass *pb.UpdateMasterClassDTO
	if err := mapstructure.Decode(updateMC, &updMasterClass); err != nil {
		log.Printf("MasterClassesService.UpdateMasterClass conversion error: %v", err)
		return nil, err
	}
	updMasterClass.AuthorId = userID

	masterClass, err := grpc_client.MasterClassesGrpcClient.UpdateMasterClass(ctx, updMasterClass)
	if err != nil {
		log.Printf("MasterClassesService.UpdateMasterClass error: %v", err)
		return nil, err
	}

	return converters.ConverterInNewMasterClassesDTO(masterClass)
}

func (s *MasterClassesService) DeleteMasterClass(
	ctx context.Context, authorID, masterClassID uint64) error {
	deleteMC := &pb.UserMasterClassID{IdAuthor: authorID, IdMc: masterClassID}
	_, err := grpc_client.MasterClassesGrpcClient.DeleteMasterClass(ctx, deleteMC)
	if err != nil {
		log.Printf("MasterClassesService.DeleteMasterClass error: %v", err)
		return err
	}
	return nil
}
