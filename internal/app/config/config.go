package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
	"log"
	"os"
)

var Cfg Config

// Config структура для хранения конфигурации
type Config struct {
	AccessSecretKey  string `env:"ACCESS_SECRET_KEY" env-required:"true"`
	RefreshSecretKey string `env:"REFRESH_SECRET_KEY" env-required:"true"`
}

// LoadConfig загружает конфигурацию из переменных окружения
func LoadConfig() error {
	// Загрузка .env файла
	err := godotenv.Load()
	if err != nil {
		log.Println("No .env file found")
	}

	err = cleanenv.ReadEnv(&Cfg)
	if err != nil {
		log.Printf("Cannot read configuration: %v", err)
		log.Printf("ACCESS_SECRET_KEY: %s", os.Getenv("ACCESS_SECRET_KEY"))   // вывод значения переменной окружения
		log.Printf("REFRESH_SECRET_KEY: %s", os.Getenv("REFRESH_SECRET_KEY")) // вывод значения переменной окружения
		return err
	}
	return nil
}
