package router

import (
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/handlers/master_classes"
	"go_master_classes_gw/internal/app/middleware"
	"go_master_classes_gw/internal/app/services"
)

func initMasterClassesHandler() *master_classes.HandlerMasterClasses {
	masterClassesService := &services.MasterClassesService{}
	masterClassesHandler := &master_classes.HandlerMasterClasses{MasterClassesService: masterClassesService}
	return masterClassesHandler
}

func MasterClassesSetupRoutes(r *gin.Engine) {
	handler := initMasterClassesHandler()
	mcGroup := r.Group("/master_classes")
	mcGroup.Use(middleware.AuthenticationMiddleware())
	{
		mcGroup.GET("", handler.GetMasterClasses)
		mcGroup.GET("/:id", handler.GetMasterClass)
		mcGroup.POST("", handler.CreateMasterClass)
	}
}

func MyMasterClassesSetupRoutes(r *gin.Engine) {
	handler := initMasterClassesHandler()
	myMCGroup := r.Group("/master_classes/mine")
	myMCGroup.Use(middleware.AuthenticationMiddleware())
	{
		myMCGroup.GET("", handler.GetMasterClassesMine)
		myMCGroup.PATCH("", handler.UpdateMasterClassMine)
		myMCGroup.DELETE("/:id", handler.DeleteMasterClassMine)
	}
}
