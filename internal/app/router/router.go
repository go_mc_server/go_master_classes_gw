// Роутеры и прокси-логика
package router

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go_master_classes_gw/internal/app/handlers"
	"go_master_classes_gw/internal/app/logs"
	"go_master_classes_gw/internal/app/middleware"
)

func SetupRoutes() *gin.Engine {
	rout := gin.Default()

	// Swagger
	rout.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	logger := logs.SetupLogrus()

	// Подключаем мидлвары
	rout.Use(middleware.Logger(logger))

	rout.GET("/tex/:text", handlers.GetTestHandler)

	// Настраиваем маршрутизацию
	//Users
	AuthSetupRoutes(rout)
	UsersAdminSetupRoutes(rout)
	UserMyselfSetupRoutes(rout)

	//MasterClasses
	MyMasterClassesSetupRoutes(rout)
	MasterClassesSetupRoutes(rout)

	//Boards
	MyBoardsSetupRoutes(rout)
	BoardsSetupRoutes(rout)

	return rout
}
