package router

import (
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/handlers/users"
	"go_master_classes_gw/internal/app/middleware"
	"go_master_classes_gw/internal/app/services"
)

func initUsersHandler() *users.HandlerUsers {
	userService := &services.UsersService{}
	userHandler := &users.HandlerUsers{UserService: userService}
	return userHandler
}

func AuthSetupRoutes(r *gin.Engine) {
	handler := initUsersHandler()
	authGroup := r.Group("")
	{
		authGroup.POST("/login", handler.LoginUser)
		authGroup.POST("/logout", handler.LogoutUser)
		authGroup.POST("/register",
			middleware.AvailabilityAuthenticationMiddleware(),
			handler.CreateUser)
		authGroup.PATCH("/password", handler.UpdatePassword)
	}
}

func UsersAdminSetupRoutes(r *gin.Engine) {
	handler := initUsersHandler()
	userGroup := r.Group("/users")
	userGroup.Use(middleware.AuthenticationMiddleware())
	{
		userGroup.GET("", handler.GetUsers)
		userGroup.GET("/:id", handler.GetUserByID)
		userGroup.PATCH("/user_role", handler.UpdateUserRole)
		userGroup.DELETE("/:id", handler.DeleteUser)
	}
}

func UserMyselfSetupRoutes(r *gin.Engine) {
	handler := initUsersHandler()
	userGroup := r.Group("/user/myself")
	userGroup.Use(middleware.AuthenticationMiddleware())
	{
		userGroup.GET("", handler.GetMyself)
		userGroup.PATCH("", handler.UpdateMyself)
	}
}
