package router

import (
	"github.com/gin-gonic/gin"
	"go_master_classes_gw/internal/app/handlers/boards"
	"go_master_classes_gw/internal/app/middleware"
	"go_master_classes_gw/internal/app/services"
)

func initBoardHandler() *boards.HandlerBoards {
	boardService := &services.BoardsService{}
	boardHandler := &boards.HandlerBoards{BoardsService: boardService}
	return boardHandler
}

func BoardsSetupRoutes(r *gin.Engine) {
	handler := initBoardHandler()
	boardsGroup := r.Group("/boards")
	boardsGroup.Use(middleware.AuthenticationMiddleware())
	{
		boardsGroup.GET("/:id", handler.GetBoardsByID)
		boardsGroup.POST("", handler.CreateBoard)
	}
}

func MyBoardsSetupRoutes(r *gin.Engine) {
	handler := initBoardHandler()
	boardsGroup := r.Group("/boards/mine")
	boardsGroup.Use(middleware.AuthenticationMiddleware())
	{
		boardsGroup.GET("", handler.GetBoardsMine)
		boardsGroup.PATCH("", handler.UpdateBoardMine)
		boardsGroup.DELETE("/:id", handler.DeleteBoardMine)
	}
}
