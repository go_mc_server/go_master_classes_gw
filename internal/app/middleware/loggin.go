// Мидлвары для логирования
package middleware

import (
	"github.com/gin-gonic/gin"
	"log"
)

func LoggingMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Println("Request received: ", c.Request.URL.Path)
		c.Next()
		log.Println("Response sent: ", c.Writer.Status())
	}
}
