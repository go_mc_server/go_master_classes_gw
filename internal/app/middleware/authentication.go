// Мидлвары для аутентификации
package middleware

import (
	"context"
	"github.com/gin-gonic/gin"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
	auth2 "go_master_classes_gw/internal/app/auth"
	"go_master_classes_gw/internal/app/enum"
	"go_master_classes_gw/internal/app/grpc_gw/grpc_client"
	"log"
	"net/http"
)

// AuthenticationMiddleware возвращает middleware для проверки авторизации
func AuthenticationMiddleware() gin.HandlerFunc {
	return authenticateRequest
}

// authenticateRequest - основная логика проверки авторизации
func authenticateRequest(c *gin.Context) {
	var accessToken, refreshToken, newAccessToken, role string
	var idUser uint
	var err error

	accessToken, err = c.Cookie("access_token")
	if err != nil {
		log.Println("Access token cookie not found")
		refreshToken, err = c.Cookie("refresh_token")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		newAccessToken, idUser, role, err = updateAccessToken(c.Request.Context(), refreshToken)
		if err != nil {
			log.Println("Access token has not been restored")
			c.Abort()
			return
		}
		log.Println("Access token has been restored")
		// Установка cookie access_token
		http.SetCookie(c.Writer, auth2.CreateAccessTokenCookie(newAccessToken))
	}

	idUser, role, err = auth2.VerifyAccessToken(accessToken)
	if err != nil {
		log.Println("Access token invalid")
		refreshToken, err = c.Cookie("refresh_token")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		newAccessToken, idUser, role, err = updateAccessToken(c.Request.Context(), refreshToken)
		if err != nil {
			log.Println("Access token has not been restored")
			c.Abort()
			return
		}
		log.Println("Access token has been restored")
		// Установка cookie access_token
		http.SetCookie(c.Writer, auth2.CreateAccessTokenCookie(newAccessToken))
	}

	c.Set("Role", role) // Устанавливаем Role в контексте
	c.Set("UsId", idUser)
	c.Next()
}

// updateAccessToken обновляем access_token возвращаем новый access_token, idUser, role, error
func updateAccessToken(ctx context.Context, refreshToken string) (string, uint, string, error) {
	id, err := auth2.VerifyRefreshToken(refreshToken)
	if err != nil {
		return "", 0, "", err
	}

	userID := pb.UserID{Id: uint64(id)}
	user, err := grpc_client.UsersGrpcClient.GetUserByID(ctx, &userID)
	if err != nil {
		return "", 0, "", err
	}

	uId := uint(user.Id)
	uRole := enum.RolesUser(user.Role)

	accessToken, err := auth2.GenerateAccessTokens(&uId, &uRole)
	if err != nil {
		return "", 0, "", err
	}
	log.Println("Сформирован новый access token")

	return accessToken, uId, string(uRole), nil
}

// проверка наличия аутентификации, если нет то устанавливаем пустую роль
func AvailabilityAuthenticationMiddleware() gin.HandlerFunc {
	return errorFreeAuthenticateRequest
}

func errorFreeAuthenticateRequest(c *gin.Context) {
	var accessToken, refreshToken, newAccessToken, role string
	var idUser uint
	var err error

	accessToken, err = c.Cookie("access_token")
	if err != nil {
		log.Println("Access token cookie not found")
		refreshToken, err = c.Cookie("refresh_token")
		if err != nil {
			log.Println("Refresh token cookie not found")
			c.Set("Role", "") // Устанавливаем пустую роль
			c.Next()
			return
		}
		newAccessToken, idUser, role, err = errorFreeUpdateAccessToken(c.Request.Context(), refreshToken)
		if err != nil {
			log.Println("Access token has not been restored")
			c.Set("Role", "") // Устанавливаем пустую роль
			c.Next()
			return
		}
		log.Println("Access token has been restored")
		// Установка cookie access_token
		http.SetCookie(c.Writer, auth2.CreateAccessTokenCookie(newAccessToken))
	}
	idUser, role, err = auth2.VerifyAccessToken(accessToken)
	if err != nil {
		log.Println("Access token invalid")
		refreshToken, err = c.Cookie("refresh_token")
		if err != nil {
			log.Println("Refresh token cookie not found")
			c.Set("Role", "") // Устанавливаем пустую роль
			c.Next()
			return
		}
		newAccessToken, idUser, role, err = errorFreeUpdateAccessToken(c.Request.Context(), refreshToken)
		if err != nil {
			log.Println("Access token has not been restored")
			c.Set("Role", "") // Устанавливаем пустую роль
			c.Next()
			return
		}
		log.Println("Access token has been restored")
		// Установка cookie access_token
		http.SetCookie(c.Writer, auth2.CreateAccessTokenCookie(newAccessToken))
	}
	c.Set("Role", role)
	c.Set("UsId", idUser)
	c.Next()
}

func errorFreeUpdateAccessToken(ctx context.Context, refreshToken string) (string, uint, string, error) {
	id, err := auth2.VerifyRefreshToken(refreshToken)
	if err != nil {
		return "", 0, "", err
	}

	userID := pb.UserID{Id: uint64(id)}
	user, err := grpc_client.UsersGrpcClient.GetUserByID(ctx, &userID)
	if err != nil {
		return "", 0, "", err
	}

	uId := uint(user.Id)
	uRole := enum.RolesUser(user.Role)
	accessToken, err := auth2.GenerateAccessTokens(&uId, &uRole)
	if err != nil {
		return "", 0, "", err
	}
	log.Println("Сформирован новый access token")

	return accessToken, uId, string(uRole), nil
}
