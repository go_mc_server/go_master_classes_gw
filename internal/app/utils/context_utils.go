package utils

import (
	"context"
	"time"
)

// EnsureTimeout проверяет, установлен ли дедлайн, и устанавливает тайм-аут, если нет
func EnsureTimeout(ctx context.Context, timeout time.Duration) (context.Context, context.CancelFunc) {
	if _, ok := ctx.Deadline(); ok {
		// Дедлайн уже установлен, возвращаем исходный контекст
		return ctx, nil
	}

	// Дедлайн не установлен, создаем контекст с тайм-аутом
	return context.WithTimeout(ctx, timeout)
}
