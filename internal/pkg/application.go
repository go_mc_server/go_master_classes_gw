package pkg

import (
	"context"
	"errors"

	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"go_master_classes_gw/internal/app/config"
	"go_master_classes_gw/internal/app/grpc_gw/grpc_client"
	"go_master_classes_gw/internal/app/router"
)

const (
	addressGRPC = "localhost:50051"
)

func StartApplication() {
	// загружаем конфигурацию из переменных окружения
	err := config.LoadConfig()
	if err != nil {
		log.Fatalf("Failed to load config: %v", err)
	}

	conn, err := grpc.NewClient(addressGRPC, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("Failed to connect to gRPC server %s: %v", addressGRPC, err)
	}

	// Функция для закрытия подключения с отложенным выполнением
	defer closeGRPCConnection(conn)

	// Инициализация gRPC-клиентов
	grpc_client.InitGRPCClients(conn)

	rout := router.SetupRoutes()

	server := &http.Server{
		Addr:    ":8080",
		Handler: rout,
	}

	var wg sync.WaitGroup

	// Запуск сервера в отдельной горутине
	wg.Add(1)
	go startServer(&wg, server)

	waitForShutdown(server, &wg)

	// Закрыть клиентов gRPC
	grpc_client.CloseGRPCClients()

	log.Println("Server exiting")
}

func startServer(wg *sync.WaitGroup, srv *http.Server) {
	defer wg.Done()
	if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatalf("listen: %s\n", err)
	}
}

func waitForShutdown(srv *http.Server, wg *sync.WaitGroup) {
	// Канал для получения системных сигналов завершения
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	// Блокируемся до получения сигнала завершения
	<-quit
	log.Println("Shutting down server...")

	// Контекст с тайм-аутом для завершения работы сервера
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Остановка HTTP-сервера
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	// Ожидание завершения всех горутин
	wg.Wait()
}

func closeGRPCConnection(conn *grpc.ClientConn) {
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Fatalf("Failed to close gRPC connection: %v", err)
		}
	}()
}
